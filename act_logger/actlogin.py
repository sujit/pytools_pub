#!/usr/bin/env python
try:
    import requests, argparse
except ImportError, msg:
    print msg
    exit(-1)

def main():
    parser = argparse.ArgumentParser(description="ACT Web Portal Logger options", usage='%(prog)s [options]')
    parser.add_argument("-u", dest="user", help="ACT UserID", type=int)
    parser.add_argument("-p", dest="password", help="ACT Login password", type=str)
    parser.add_argument("--version", action="version", version="ACT Portal Logger v1.1")

    results = parser.parse_args()

    loginData   = {"act_username" : results.user, "act_password" : results.password}
    url         = "http://portal.acttv.in"
    headData    = { \
            "Host"              :   "portal.acttv.in", \
            "User-Agent"        :   "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/36.0.1985.125", \
            "Referer"           :   "http://portal.acttv.in/", \
            "Content-Type"      :   "application/x-www-form-urlencoded", \
            "Connection"        :   "keep-alive", \
            "Accept-Encoding"   :   "gzip,deflate,sdch", \
            "Origin"            :   "http://portal.acttv.in"
            }

    if not (results.user and results.password):
        parser.print_help()

    # Initialize the login routine 
    if (results.user and results.password):
        print '[+] UserID:\t', results.user
        print '[+] Password:\t', results.password
        initQuery = requests.get(url, headers=headData)
        if (initQuery.status_code == 200 and initQuery.text.find('You have invalid network configuration')) > -1:
            print '[-] Error: Network outside of ACT perimeter. Sorry, can\'t login.'
            exit(-1)

        if (initQuery.status_code == 200 and initQuery.text.find('ACTBroadbandOfficial')) > -1:
            print '[*] Valid ACT Login page found!'
            print '[*] Logging in to the ACT Portal...'
            loginQuery = requests.post(url, headers=headData, data=loginData)
            if (loginQuery.status_code == 200 and loginQuery.headers["transfer-encoding"] == "chunked" and loginQuery.text.find('You are logged in as<br') > -1):
                print '[*] Logged in successfully to ACT portal!'
                print '[*] Enjoy ...'
            else:
                print '[-] Unknown error encountered...'

if __name__ == '__main__':
    main()
