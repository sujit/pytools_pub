#########################################################################
# Name 		:		Mediafire Download Links Generator
# Module 	:		Tool to generate direct DL Links from MediaFire Host	
# Author 	:		Sujit Ghosal
# Email 	: 		thesujit [AT] gmail [dot] com
# Date 		:		10th August 2014 
# Version  	:		1.0
#########################################################################
import sys, re, os
#from pprint import pprint

try:
	from BeautifulSoup import BeautifulSoup as BS
except ImportError:
	print '[-]BeautifulSoup module missing. Please install this module and try again.'
	print '[-]Module location: http://bit.ly/1mADXsB'
	sys.exit(0)

try:
	import requests
except ImportError:
	print '[-]Requests module missing. Please install this module and try again.'
	print '[-]Module location: http://bit.ly/1kUBi23'
	sys.exit(0)

headerData = {'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 Chrome/37.0.2062.68'}

def readMediafireURLs():
	'''
	This module will read links.txt for all the Mediafire URLs and further inspect them. Later on
	after multiple level of validations, this function will generate the list of direct download links
	accordingly.  
	'''
	allLinks = []					# Push all the retrieved Mediafire URLs here
	if os.path.isfile('links.txt') is False:
		print '[-]Error: Input file \'links.txt\' is missing!'
		sys.exit(0)

	readFile 	= open('links.txt', 'r')
	print '[+]Validating URLs. Please wait ...'
	for eachLine in readFile:
		if eachLine.startswith('http://www.mediafire.com/?') is True:
			#print '[+]URL: %s' % (eachLine),
			payload 	= {'url' : eachLine, 'submit' : 'Check+Links'}
			reqObj  	= requests.post('http://urlchecker.org', headers=headerData, data=payload)
			rspData 	= reqObj.text.encode('utf-8')

			matches = re.findall('http://www.mediafire.com/\?[0-9a-zA-Z]{15}', rspData)
			for i in matches:
				allLinks.insert(0, i)

		elif  eachLine.startswith('http://www.mediafire.com/?') is False:
			print '[-]Invalid Mediafire URL found. Skipped URL:', eachLine
			pass

		else:
			print '[-]ERROR: Unable to find any Mediafire URLs.'
			sys.exit(0)

	return allLinks


def parsePages():
	'''
	This module will crawl through Mediafire website and search for all the files
	present which needs to be opened through each individual session one-by-one.
	Afterwards this function validate the HTML webpage for each resource entity and
	list the direct download links found for all the links found in that website. 
	'''
	urls = readMediafireURLs()			# Contains the list of all URLs
	print '[+]%s URLs found.' % len(urls)
	print '[+]Retrieving download links ...'

	flag = 0
	dlinks = []							# All download links needs to be pushed here

	for eachURL in urls:
		reqObj 		= 	requests.get(eachURL, headers=headerData)
		soupObj    	= 	BS(reqObj.text)
		classMatch 	= 	soupObj.findAll('div', {'class' : 'download_link'})

		for check in classMatch:
			allStr = check.getText().encode('utf-8')
			if allStr.find('http://download') > 0:
				pattern = re.findall('http://download*.*', allStr)
				httpURI = pattern[0].strip('\";')
				if (len(httpURI) > 0) and (httpURI.startswith('http://download')):
					dlinks.insert(0, httpURI)
			
			elif allStr.find('http://download') == 0:
				print '[-]Unable to retrieve any URLs.'
				sys.exit(0)

	return dlinks

def saveLog():
	'''
	This module will retrieve the list of generated mediafire direct download URLs and export 
	them to mediafire_direct_download_urls_list log file for each reference. Later on this file 
	can be used as an input against wget or curl to download all the target resource entities.
	'''
	finURLs = parsePages()
	if os.path.isfile('mediafire_direct_download_urls_list.log') is True:
		os.remove('mediafire_direct_download_urls_list.log')
		for eachLine in finURLs:
			fileObj = open('mediafire_direct_download_urls_list.log', 'a')
			fileObj.writelines(eachLine + '\n')

	if os.path.isfile('mediafire_direct_download_urls_list.log') is False:
		for eachLine in finURLs:
			fileObj = open('mediafire_direct_download_urls_list.log', 'a')
			fileObj.writelines(eachLine + '\n')

	print '[+]DL links are exported to: mediafire_direct_download_urls_list.log'
	print '[+]Enjoy!'


if __name__ == '__main__':
	if len(sys.argv) == 1:
		saveLog()
	else:
		print '[+]Usage:$ python % <return>'
		sys.exit(0)